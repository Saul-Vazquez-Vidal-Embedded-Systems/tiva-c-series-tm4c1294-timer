#include<stdint.h>
#include<stdbool.h>
#include "inc/tm4c1294ncpdt.h"

void Timer03AIntHandler(void);
void complementa3A(void);
void IntPortJ(void);
void setupT1(void);
void setupT2(void);
void setupT3(void);
void setupT4(void);


void IntPortJ(void){
    static int current_period = 1;

    if((GPIO_PORTJ_AHB_RIS_R & 0x01) == 0x01){
        if(current_period == 4){
            current_period = 1;
        }
        else{
            current_period++;
        }

        switch(current_period){
            case 1:
                setupT1();
                break;
            case 2:
                setupT2();
                break;
            case 3:
                setupT3();
                break;
            case 4:
                setupT4();
                break;
        }

        GPIO_PORTJ_AHB_ICR_R |= 0x01;
    }
}

void Timer03AIntHandler(void)
{
    //LIMPIA BANDERA
    TIMER3_ICR_R= 0x0001; //LIMPIA BANDERA DE TIMER3
    // llama a la funci�n complemento de leds.
    complementa3A();
}

void setupT1(void){
    //DESHABILITA EL TIMER PARA RECONFIGURARLO
    TIMER3_CTL_R=0X00000000;
    //SE MODIFICA EL VALOR DEL PREMULTIPLICADOR
    TIMER3_TAPR_R= 0x0C;
    //SE MODIFICA EL VALOR DE RECARGA
    TIMER3_TAILR_R= 0x3500;
    //SE VUELVE A HABILITAR EL TIMER
    TIMER3_CTL_R |= 0x00000001;
}

void setupT2(void){
    //DESHABILITA EL TIMER PARA RECONFIGURARLO
    TIMER3_CTL_R=0X00000000;
    //SE MODIFICA EL VALOR DEL PREMULTIPLICADOR
    TIMER3_TAPR_R= 0x30;
    //SE MODIFICA EL VALOR DE RECARGA
    TIMER3_TAILR_R= 0xD400;
    //SE VUELVE A HABILITAR EL TIMER
    TIMER3_CTL_R |= 0x00000001;
}

void setupT3(void){
    //DESHABILITA EL TIMER PARA RECONFIGURARLO
    TIMER3_CTL_R=0X00000000;
    //SE MODIFICA EL VALOR DEL PREMULTIPLICADOR
    TIMER3_TAPR_R= 0x55;
    //SE MODIFICA EL VALOR DE RECARGA
    TIMER3_TAILR_R= 0x7300;
    //SE VUELVE A HABILITAR EL TIMER
    TIMER3_CTL_R |= 0x00000001;
}

void setupT4(void){
    //DESHABILITA EL TIMER PARA RECONFIGURARLO
    TIMER3_CTL_R=0X00000000;
    //SE MODIFICA EL VALOR DEL PREMULTIPLICADOR
    TIMER3_TAPR_R= 0x7A;
    //SE MODIFICA EL VALOR DE RECARGA
    TIMER3_TAILR_R= 0x1200;
    //SE VUELVE A HABILITAR EL TIMER
    TIMER3_CTL_R |= 0x00000001;
}

void complementa3A(void){
    GPIO_PORTN_DATA_R ^= 0x02;
}

int main(void)
{
    //---------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------CONFIGURACION DE LOS PUERTOS --------------------------------------------------------------------
    //Se habilitan los puetos J y N
    SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R12 | SYSCTL_RCGCGPIO_R8;

    //CONFIGURACION DEL PUERTO N
    GPIO_PORTN_DATA_R = 0x00;   //APAGAMOS LOS LEDS
    GPIO_PORTN_DIR_R = 0x02;    //PUSH 1 COMO SALIDA
    GPIO_PORTN_DEN_R = 0x02;    //DIGITAL

    //CONFIGURACION DEL PUERTO J
    GPIO_PORTJ_AHB_DEN_R |= 0x01;   //DIGITALES
    GPIO_PORTJ_AHB_DIR_R |= 0x00;   //ENTRADAS
    GPIO_PORTJ_AHB_PUR_R |= 0x01;   //RESISTENCIA PULL UP
    GPIO_PORTJ_AHB_IM_R |= 0x01;    //PUERTO J HABILITADO PARA INTERRUPCION
    GPIO_PORTJ_AHB_IS_R |= 0x00;    //DETECCION DE INTERRUPCION POR FLANCO
    GPIO_PORTJ_AHB_IEV_R |= 0x00;   //DE BAJADA

    //---------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------- CONFIGURACION GENERAL DE TIMER ----------------------------------------------------------------------
    //SE HABILITA EL TIMER 3
    SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R3; ////Pag 380

    //RETARDO PARA QUE EL RELOJ ALCANCE EL TIMER 3
    while((SYSCTL_PRTIMER_R & SYSCTL_RCGCTIMER_R3)!= SYSCTL_RCGCTIMER_R3); //PAG 497.

    //DESHABILITA EL TIMER 3 PARA CONFIGURARLO
    TIMER3_CTL_R=0X00000000; //PAG 986.

    //SE CONFIGURARA PARA MODO DE 16 BITS
    TIMER3_CFG_R= 0X00000004; //PAG 976.

    //---------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------- CONFIGURACION TIMER A -------------------------------------------------------------------------
    //MODO PERIODICO HACIA ARRIBA. PAG. 977
    TIMER3_TAMR_R= 0x00000012;  //TIMER3_TBMR_R = TIMER3_TBMR_R | 0x02 (ONE-SHOT/PERIODIC) | 0x10 (UP/DOWN)

    //PAG. 1004
    TIMER3_TAILR_R= 0x3500;     //Valor de recarga = 0x3500

    //PAG. 1008.
    TIMER3_TAPR_R= 0x0C;        //Premultiplicador = 0x0C
    //---------------------------------------------------------------------------------------------------------------------------------------

    //LIMPIAR POSIBLE BANDERA PENDIENTE DEL TIMER 3 A
    TIMER3_ICR_R = 0x00000001; //LIMPIA BANDERA. PAG 1002

    //ACTIVAR INTERRUPCIONES DE TIMEOUT
    TIMER3_IMR_R |= 0x00000001; //HABILITA LA INTERRUPCION DE  TIMER 3 A. PAG. 993

    //SE HABILITA EL TIMER 3 A
    TIMER3_CTL_R |= 0x00000001; //HABILITA TIMERA EN LA CONFIGURACION
    //---------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------- HABILITACION DE TIMER PARA NVIC ----------------------------------------------------------------
    //PAG. 154 (ENABLE registers) y 117 (Interrupt table)
    NVIC_EN1_R |= 0x00000008;////Se esta habilitando la interrupcion 35 del timer 3A. Que esta en EN1 = 0000 0000 0000 1000
    NVIC_EN1_R |= 0x00080000;//Se habilita PORTJ
    //---------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------

    while(1);

}
